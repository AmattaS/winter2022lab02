import java.util.Scanner;
public class PartThree
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter square side");
		//Setting the variables
		double sqr = sc.nextDouble();
		
		System.out.println("Enter length of rectangle");
		
		double length = sc.nextDouble();
		
		System.out.println("Enter width of rectangle");
		
		double width = sc.nextDouble();
		//Calling methods
		AreaComputations aSquare = new AreaComputations();
		System.out.println("Area of square is "+aSquare.areaSquare(sqr));
		
		System.out.println("Area of rectangle is "+aSquare.areaRectangle(length, width));
		
	}
	
	
}