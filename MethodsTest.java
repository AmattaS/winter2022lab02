public class MethodsTest
{
	public static void main(String[] args)
	{	
		
		int x = 10;
		System.out.println(x);
		methodNoInputReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		
		methodTwoInputNoReturn(1, 1.5);
		
		int z = methodNoInputReturnInt();
		System.out.println("return int method "+z);
		
		double res = sumSquareRoot(6,3);
		System.out.println("squareroot method "+res);
		
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println("s1 lenght "+s1.length());
		System.out.println("s2 length "+s2.length());
		
		//Secondclass
		SecondClass sc = new SecondClass();
		System.out.println("Secondclass addtwo method "+ sc.addTwo(50));
		
		
	}
	public static void methodNoInputReturn() 
	{	
		//System.out.println("I;m in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int var)
	{
		System.out.println("Inside the method one input no return "+var);
		
		
	}
	
	public static void methodTwoInputNoReturn(int a, double b)
	{
		
	}
	
	public static int methodNoInputReturnInt()
	{
		int six = 6;
		return six;
	}
	
	public static double sumSquareRoot(int a, int b)
	{
		double c = a+b;
		Math.sqrt(c);
		return c;
	}
}